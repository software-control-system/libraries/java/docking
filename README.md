<p style="text-align: center"><a href="https://www.synchrotron-soleil.fr/en"><img src="https://www.synchrotron-soleil.fr/sites/default/files/logo_0.png" alt="SOLEIL Synchrotron"/></a></p>

A project for docking management.


---
# What is docking?
docking is a system in which the user can dynamically change the layout of the application

# Organization and licenses
docking project is organized into maven modules.  
Each module has its own license.
1. **dockingcore**
    * dockingcore is the module that describes eveything needed to do some docking in your application.  
    It proposes some interfaces and abstract classes, the implementation/concretization of which is done in other modules.  
    This way, it gives some common way to manage docking without knowning the implementation library.  
    * It is licensed under LGPL V3.
1. **dockinginfonode**
    * Concretization of dockingcore using [InfoNode Docking Windows](https://sourceforge.net/projects/infonode/).  
    * It is licensed under GPL V3.
1. **dockingvl**
    * Concretization of dockingcore using a fork of [VLDocking](https://bitbucket.org/akuhtz/vldocking/src/master/).  
    * It is licensed under GPL V3.
