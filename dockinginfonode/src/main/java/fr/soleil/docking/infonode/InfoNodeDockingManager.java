/*
 * This file is part of dockinginfonode.
 * 
 * dockinginfonode is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * dockinginfonode is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with dockinginfonode. If not, see
 * <https://www.gnu.org/licenses/>
 */
package fr.soleil.docking.infonode;

import java.awt.Color;
import java.beans.PropertyChangeEvent;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import javax.swing.JComponent;

import fr.soleil.docking.ADockingManager;
import fr.soleil.docking.exception.DockingException;
import fr.soleil.docking.infonode.view.InfoNodeViewFactory;
import fr.soleil.docking.perspective.IPerspective;
import fr.soleil.docking.perspective.IPerspectiveFactory;
import fr.soleil.docking.perspective.PerspectiveFactory;
import fr.soleil.docking.view.IView;
import fr.soleil.docking.view.IViewFactory;
import net.infonode.docking.RootWindow;
import net.infonode.docking.View;
import net.infonode.docking.ViewSerializer;
import net.infonode.docking.properties.DockingWindowProperties;
import net.infonode.docking.util.DockingUtil;
import net.infonode.util.Direction;

/**
 * A class that prepares docking. Obtain your RootWindow from this class.
 * 
 * @author HARDION
 * @author GIRARDOT
 */
public class InfoNodeDockingManager extends ADockingManager {

    protected RootWindow rootWindow;

    public InfoNodeDockingManager() {
        this(new InfoNodeViewFactory(), new PerspectiveFactory());
    }

    public InfoNodeDockingManager(IViewFactory viewFactory) {
        this(viewFactory, new PerspectiveFactory());
    }

    public InfoNodeDockingManager(IViewFactory viewFactory, IPerspectiveFactory perspectiveFactory) {
        super(viewFactory, perspectiveFactory);
    }

    @Override
    protected void initDockingArea() {
        rootWindow = (RootWindow) createNewDockingArea(null);
    }

    @Override
    public JComponent getDockingArea() {
        if (rootWindow == null) {
            initDockingArea();
        }
        return rootWindow;
    }

    @Override
    public JComponent createNewDockingArea(Color background) {
        RootWindow result = generateRootWindow();
        if (background != null) {
            result.getRootWindowProperties().getWindowAreaProperties().setBackgroundColor(background);
        }
        updateRootWindowAfterViewAdding(result);
        return result;
    }

    protected RootWindow generateRootWindow() {
        return new RootWindow(new MyViewSerializer(viewFactory));
    }

    protected void updateRootWindowAfterViewAdding(RootWindow window) {
        window.getWindowBar(Direction.DOWN).setEnabled(true);
        window.getRootWindowProperties().getDockingWindowProperties().setUndockEnabled(false);
    }

    @Override
    public void setDockingAreaBeackground(JComponent dockingArea, Color areaBackground) {
        if (dockingArea instanceof RootWindow) {
            RootWindow rootWindow = (RootWindow) dockingArea;
            rootWindow.getRootWindowProperties().getWindowAreaProperties().setBackgroundColor(areaBackground);
        }
    }

    @Override
    public void applyPerspective(IPerspective perspective, JComponent dockingArea) throws DockingException {
        DockingException dockingException = null;
        if ((perspective != null) && (perspective.getByteArray() != null) && (perspective.getByteArray().length > 0)
                && (dockingArea instanceof RootWindow)) {
            RootWindow rootWindow = (RootWindow) dockingArea;
            ObjectInputStream ois = null;
            ByteArrayInputStream bais = null;
            try {
                bais = new ByteArrayInputStream(perspective.getByteArray());
                ois = new ObjectInputStream(bais);
                rootWindow.read(ois);
            } catch (Exception e) {
                dockingException = new DockingException(
                        getClass().getSimpleName() + ".applyPerspective(): Unexpected Error", e);
            } finally {
                try {
                    if (ois != null) {
                        ois.close();
                    }
                    // Actually there is no effect to close bais ...
                } catch (IOException e) {
                    if (dockingException == null) {
                        dockingException = new DockingException("I/O Exception", e);
                    } else {
                        e.printStackTrace();
                    }
                }
            }
        }
        if (dockingException != null) {
            throw dockingException;
        }
    }

    @Override
    public void updatePerspective(IPerspective perspective, JComponent dockingArea) throws DockingException {
        DockingException dockingException = null;
        if ((perspective != null) && (dockingArea instanceof RootWindow)) {
            RootWindow rootWindow = (RootWindow) dockingArea;
            ByteArrayOutputStream baos = null;
            ObjectOutputStream ous = null;
            baos = new ByteArrayOutputStream();
            try {
                ous = new ObjectOutputStream(baos);
                rootWindow.write(ous, false);
            } catch (IOException e) {
                dockingException = new DockingException(
                        getClass().getSimpleName() + ".updatePerspective(): Unexpected Error", e);
            } finally {
                try {
                    ous.close();
                    // Actually there is no effect to close baos ...
                } catch (IOException e) {
                    if (dockingException == null) {
                        dockingException = new DockingException("I/O Exception", e);
                    } else {
                        e.printStackTrace();
                    }
                }
            }
            perspective.setByteArray(baos.toByteArray());
        }
        if (dockingException != null) {
            throw dockingException;
        }
    }

    @Override
    protected void updateViews(PropertyChangeEvent evt) {
        if (evt.getSource() == this.viewFactory) {
            View newView = (View) evt.getNewValue();
            View oldView = (View) evt.getOldValue();
            if (newView == null) { // removed
                oldView.close();
            } else {
                DockingUtil.addWindow(newView, rootWindow);
            }
        }
    }

    @Override
    public void setEnabledDocking(boolean enabledDocking, JComponent dockingArea) {
        if (dockingArea instanceof RootWindow) {
            RootWindow rootWindow = (RootWindow) dockingArea;
            rootWindow.getRootWindowProperties().getDockingWindowProperties().setCloseEnabled(enabledDocking);
            rootWindow.getRootWindowProperties().getDockingWindowProperties().setDragEnabled(enabledDocking);
            rootWindow.getRootWindowProperties().getDockingWindowProperties().setMaximizeEnabled(enabledDocking);
            rootWindow.getRootWindowProperties().getDockingWindowProperties().setMinimizeEnabled(enabledDocking);
        }
    }

    @Override
    public boolean isEnabledDocking(JComponent dockingArea) {
        boolean enabled;
        if (dockingArea instanceof RootWindow) {
            RootWindow rootWindow = (RootWindow) dockingArea;
            DockingWindowProperties dwp = rootWindow.getRootWindowProperties().getDockingWindowProperties();
            enabled = dwp.getCloseEnabled() && dwp.getDragEnabled() && dwp.getMaximizeEnabled()
                    && dwp.getMinimizeEnabled();
        } else {
            enabled = false;
        }
        return enabled;
    }

    @Override
    public void setUndockEnabled(boolean enabled, JComponent dockingArea) {
        if (dockingArea instanceof RootWindow) {
            RootWindow rootWindow = (RootWindow) dockingArea;
            rootWindow.getRootWindowProperties().getDockingWindowProperties().setUndockEnabled(enabled);
        }
    }

    @Override
    public boolean isUndockEnabled(JComponent dockingArea) {
        boolean enabled;
        if (dockingArea instanceof RootWindow) {
            RootWindow rootWindow = (RootWindow) dockingArea;
            enabled = rootWindow.getRootWindowProperties().getDockingWindowProperties().getUndockEnabled();
        } else {
            enabled = false;
        }
        return enabled;
    }

    @Override
    public boolean canRestoreExactViewPosition() {
        return true;
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    protected class MyViewSerializer implements ViewSerializer {

        private final IViewFactory viewFactory;

        /**
         * Utility constructor that creates a map with a number of views. A view gets it's index in
         * the array as id.
         * 
         * @param views the views to add to the map
         */
        public MyViewSerializer(IViewFactory viewFactory) {
            this.viewFactory = viewFactory;
        }

        @Override
        public View readView(ObjectInputStream in) throws IOException {
            Object id = viewFactory.readViewId(in);
            return (View) viewFactory.getView(id);
        }

        @Override
        public void writeView(View view, ObjectOutputStream out) throws IOException {
            viewFactory.writeViewId(((IView) view).getId(), out);
        }

    }

}
