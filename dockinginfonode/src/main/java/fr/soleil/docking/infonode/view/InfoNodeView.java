/*
 * This file is part of dockinginfonode.
 * 
 * dockinginfonode is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * dockinginfonode is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with dockinginfonode. If not, see
 * <https://www.gnu.org/licenses/>
 */
package fr.soleil.docking.infonode.view;

import java.awt.Color;
import java.awt.Component;
import java.util.List;

import javax.swing.Icon;

import fr.soleil.docking.event.ViewEvent;
import fr.soleil.docking.listener.IViewListener;
import fr.soleil.docking.listener.ViewListenerDelegate;
import fr.soleil.docking.view.IView;
import net.infonode.docking.AbstractTabWindow;
import net.infonode.docking.DockingWindow;
import net.infonode.docking.DockingWindowAdapter;
import net.infonode.docking.RootWindow;
import net.infonode.docking.View;

/**
 * A dynamically created view containing an id.
 * 
 * @author Hardion
 * @author GIRARDOT
 */
public class InfoNodeView extends View implements IView {

    private static final long serialVersionUID = 6381456606851418094L;

    protected Object id;
    protected final ViewListenerDelegate delegate;

    /**
     * Constructor.
     * 
     * @param title the view title
     * @param icon the view icon
     * @param component the view component
     * @param id the view id
     */
    public InfoNodeView(String title, Icon icon, Component component, Object id) {
        super(title, icon, component);
        this.id = id;
        delegate = new ViewListenerDelegate();
        addListener(new DockingWindowAdapter() {

            @Override
            public void windowClosed(DockingWindow window) {
                delegate.warnListeners(new ViewEvent(InfoNodeView.this, ViewEvent.VIEW_CLOSED));
            }

            @Override
            public void viewFocusChanged(View previouslyFocusedView, View focusedView) {
                if (InfoNodeView.this.equals(focusedView)) {
                    delegate.warnListeners(new ViewEvent(InfoNodeView.this, ViewEvent.FOCUS_GAINED));
                } else {
                    delegate.warnListeners(new ViewEvent(InfoNodeView.this, ViewEvent.FOCUS_LOST));
                }
            }
        });

    }

    @Override
    public Object getId() {
        return id;
    }

    @Override
    public boolean isVisible() {
        boolean visible;
        if (isShowing()) {
            visible = true;
        } else if ((getWindowParent() != null) && getWindowParent().isRestorable()) {
            visible = true;
        } else {
            visible = false;
        }
        return visible;
    }

    @Override
    public void setVisible(boolean visible) {
        if (visible) {
            super.restore();
        } else {
            super.close();
        }
        super.setVisible(visible);
        super.setEnabled(visible);
    }

    @Override
    public void select() {
        recursiveSelect(this);
        // requestFocusInWindow();
        // Component comp = getComponent();
        // if (comp != null) {
        // comp.requestFocusInWindow();
        // }
    }

    protected static void recursiveSelect(DockingWindow window) {
        if (window != null) {
            DockingWindow parent = window.getWindowParent();
            if (parent instanceof AbstractTabWindow) {
                AbstractTabWindow tabWindow = (AbstractTabWindow) parent;
                int index = parent.getChildWindowIndex(window);
                if (index > -1) {
                    tabWindow.setSelectedTab(index);
                }
            }
            recursiveSelect(parent);
        }
    }

    @Override
    protected void update() {
        super.update();
        super.setVisible(true);
        super.setEnabled(true);
    }

    @Override
    public Color getViewBackground() {
        return getBackground();
    }

    @Override
    public void setViewBackground(Color bg) {
        setComponentsBackground(getCustomTabComponents(), bg);
        setComponentsBackground(getCustomTitleBarComponents(), bg);
        setBackground(this, bg);
    }

    @Override
    public Color getViewForeground() {
        return getForeground();
    }

    @Override
    public void setViewForeground(Color fg) {
        setComponentsForeground(getCustomTabComponents(), fg);
        setComponentsForeground(getCustomTitleBarComponents(), fg);
        setForeground(this, fg);
    }

    /**
     * Sets the background {@link Color} of a {@link DockingWindow}
     * 
     * @param win The {@link DockingWindow}
     * @param bg The background {@link Color} to set
     */
    protected void setBackground(DockingWindow win, Color bg) {
        win.setBackground(bg);
        win.setBorder(null);
        win.getWindowProperties().getTabProperties().getTitledTabProperties().getNormalProperties()
                .getComponentProperties().setBackgroundColor(bg);
        win.getWindowProperties().getTabProperties().getTitledTabProperties().getHighlightedProperties()
                .getComponentProperties().setBackgroundColor(bg);
    }

    /**
     * Sets the foreground {@link Color} of a {@link DockingWindow}
     * 
     * @param win The {@link DockingWindow}
     * @param fg The foreground {@link Color} to set
     */
    protected void setForeground(DockingWindow win, Color fg) {
        win.setForeground(fg);
        win.getWindowProperties().getTabProperties().getTitledTabProperties().getNormalProperties()
                .getComponentProperties().setForegroundColor(fg);
        win.getWindowProperties().getTabProperties().getTitledTabProperties().getHighlightedProperties()
                .getComponentProperties().setForegroundColor(fg);
    }

    /**
     * Sets the background of all {@link Component}s in a given {@link List}
     * 
     * @param components The {@link List}
     * @param bg The background to set
     */
    protected void setComponentsBackground(List<?> components, Color bg) {
        if (components != null) {
            for (Object comp : components) {
                if (comp instanceof Component) {
                    ((Component) comp).setBackground(bg);
                }
            }
        }
    }

    /**
     * Sets the foreground of all {@link Component}s in a given {@link List}
     * 
     * @param components The {@link List}
     * @param bg The foreground to set
     */
    protected void setComponentsForeground(List<?> components, Color bg) {
        if (components != null) {
            for (Object comp : components) {
                if (comp instanceof Component) {
                    ((Component) comp).setForeground(bg);
                }
            }
        }
    }

    @Override
    public void setClosable(boolean closable) {
        setClosable(this, closable, true);
    }

    /**
     * Methods that allow or not a {@link DockingWindow} to be closable. If the {@link DockingWindow} is set as not
     * closable, all its ancestors will be set as unclosable too.
     * 
     * @param window The {@link DockingWindow} to set closable/unclosable
     * @param closable Whether the {@link DockingWindow} should be closable
     * @param updateRootWindowToo Whether to update root window properties too. Will have effect only if
     *            <code>closable</code> is <code>false</code>.
     */
    protected static void setClosable(DockingWindow window, boolean closable, boolean updateRootWindowToo) {
        if ((window != null) && (window.getWindowProperties() != null)) {
            window.getWindowProperties().getTabProperties().getNormalButtonProperties().getCloseButtonProperties()
                    .setVisible(closable);
            window.getWindowProperties().getTabProperties().getHighlightedButtonProperties().getCloseButtonProperties()
                    .setVisible(closable);
            window.getWindowProperties().setCloseEnabled(closable);
            if (!closable) {
                if (updateRootWindowToo) {
                    RootWindow rootWindow = window.getRootWindow();
                    if ((rootWindow != null) && (rootWindow.getRootWindowProperties() != null)) {
                        rootWindow.getRootWindowProperties().getDockingWindowProperties().setCloseEnabled(closable);
                    }
                }
                setClosable(window.getWindowParent(), closable, false);
            }
        }
    }

    @Override
    public void addViewListener(final IViewListener listener) {
        delegate.addViewListener(listener);
    }

    @Override
    public void removeViewListener(IViewListener listener) {
        delegate.removeViewListener(listener);
    }

    @Override
    public void setTitle(String title) {
        getViewProperties().setTitle(title == null ? "" : title);
        RootWindow rootWindow = getRootWindow();
        if (rootWindow != null) {
            rootWindow.repaint();
        }
    }

    @Override
    public void setIcon(Icon icon) {
        getViewProperties().setIcon(icon);
        RootWindow rootWindow = getRootWindow();
        if (rootWindow != null) {
            rootWindow.repaint();
        }
    }

}
