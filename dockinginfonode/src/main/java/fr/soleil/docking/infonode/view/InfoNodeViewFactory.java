/*
 * This file is part of dockinginfonode.
 * 
 * dockinginfonode is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * dockinginfonode is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with dockinginfonode. If not, see
 * <https://www.gnu.org/licenses/>
 */
package fr.soleil.docking.infonode.view;

import java.awt.Component;

import javax.swing.Icon;
import javax.swing.JComponent;

import fr.soleil.docking.ADockingManager;
import fr.soleil.docking.infonode.InfoNodeDockingManager;
import fr.soleil.docking.view.AbstractViewFactory;
import fr.soleil.docking.view.IView;
import net.infonode.docking.RootWindow;
import net.infonode.docking.util.DockingUtil;

/**
 * InfoNode implementation of {@link AbstractViewFactory}
 * 
 * @author girardot
 */
public class InfoNodeViewFactory extends AbstractViewFactory {

    public InfoNodeViewFactory() {
        super();
    }

    @Override
    protected IView createView(String title, Icon icon, Component component, Object id) {
        return new InfoNodeView(title, icon, component, id);
    }

    @Override
    protected void updateViewForDockingArea(IView view, JComponent dockingArea) {
        if ((view instanceof InfoNodeView) && (dockingArea instanceof RootWindow)) {
            InfoNodeView infoNodeView = (InfoNodeView) view;
            RootWindow rootWindow = (RootWindow) dockingArea;
            infoNodeView.getWindowProperties().getTabProperties().getNormalButtonProperties().getCloseButtonProperties()
                    .setVisible(false);
            infoNodeView.getWindowProperties().getTabProperties().getHighlightedButtonProperties()
                    .getCloseButtonProperties().setVisible(false);
            DockingUtil.addWindow(infoNodeView, rootWindow);
        }
    }

    @Override
    public ADockingManager generateDockingManager() {
        return new InfoNodeDockingManager(this);
    }

}
