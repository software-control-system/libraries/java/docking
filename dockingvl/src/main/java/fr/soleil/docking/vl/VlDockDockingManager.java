/*
 * This file is part of dockingvl.
 * 
 * dockingvl is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * dockingvl is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with dockingvl. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.docking.vl;

import java.awt.Color;
import java.beans.PropertyChangeEvent;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.swing.JComponent;
import javax.swing.UIManager;

import com.vldocking.swing.docking.DockingDesktop;
import com.vldocking.swing.docking.DockingPreferences;

import fr.soleil.docking.ADockingManager;
import fr.soleil.docking.exception.DockingException;
import fr.soleil.docking.perspective.IPerspective;
import fr.soleil.docking.perspective.IPerspectiveFactory;
import fr.soleil.docking.perspective.PerspectiveFactory;
import fr.soleil.docking.view.IViewFactory;
import fr.soleil.docking.vl.view.VlDockView;
import fr.soleil.docking.vl.view.VlDockViewFactory;

public class VlDockDockingManager extends ADockingManager {
    protected SoleilDockingDesktop mainDockingDesktop;

    public VlDockDockingManager() {
        this(new VlDockViewFactory(), new PerspectiveFactory());
    }

    public VlDockDockingManager(IViewFactory viewFactory) {
        this(viewFactory, new PerspectiveFactory());
    }

    public VlDockDockingManager(IViewFactory viewFactory, IPerspectiveFactory perspectiveFactory) {
        super(viewFactory, perspectiveFactory);
        DockingPreferences.setShadowDesktopStyle();
        UIManager.getDefaults().put("TabbedDockableContainer.tabPlacement", 1);
    }

    @Override
    protected void initDockingArea() {
        mainDockingDesktop = new SoleilDockingDesktop();
        mainDockingDesktop.setOpaqueContents(true);
    }

    @Override
    public JComponent getDockingArea() {
        if (mainDockingDesktop == null) {
            initDockingArea();
        }
        return mainDockingDesktop;
    }

    public DockingDesktop getDockingDesktop() {
        return (DockingDesktop) getDockingArea();
    }

    @Override
    public JComponent createNewDockingArea(Color background) {
        SoleilDockingDesktop result = new SoleilDockingDesktop(false, false, true, false);
        if (background != null) {
            result.setBackground(background);
        }
        return result;
    }

    @Override
    public void setDockingAreaBeackground(JComponent dockingArea, Color areaBackground) {
        if (dockingArea != null) {
            dockingArea.setBackground(areaBackground);
        }
    }

    @Override
    public void applyPerspective(IPerspective perspective, JComponent dockingArea) throws DockingException {
        DockingException dockingException = null;
        if ((perspective != null) && (perspective.getByteArray() != null) && (perspective.getByteArray().length > 0)
                && (dockingArea instanceof SoleilDockingDesktop)) {
            SoleilDockingDesktop mainDockingDesktop = (SoleilDockingDesktop) dockingArea;
            InputStream ois = null;
            ByteArrayInputStream bais = null;
            try {
                bais = new ByteArrayInputStream(perspective.getByteArray());
                // ois = new ObjectInputStream(bais);
                ois = new BufferedInputStream(bais);
                mainDockingDesktop.readXML(ois);
            } catch (Exception e) {
                dockingException = new DockingException(
                        getClass().getSimpleName() + ".applyPerspective(): Unexpected Error", e);
            } finally {
                try {
                    if (ois != null) {
                        ois.close();
                    }
                    // Actually there is no effect to close bais ...
                } catch (IOException e) {
                    if (dockingException == null) {
                        dockingException = new DockingException("I/O Exception", e);
                    } else {
                        e.printStackTrace();
                    }
                }
            }
        }
        if (dockingException != null) {
            throw dockingException;
        }
    }

    @Override
    public void updatePerspective(IPerspective perspective, JComponent dockingArea) throws DockingException {
        DockingException dockingException = null;
        if ((perspective != null) && (dockingArea instanceof SoleilDockingDesktop)) {
            SoleilDockingDesktop mainDockingDesktop = (SoleilDockingDesktop) dockingArea;
            ByteArrayOutputStream baos = null;
            OutputStream ous = null;
            baos = new ByteArrayOutputStream();
            try {
                // ous = new ObjectOutputStream(baos);
                ous = new BufferedOutputStream(baos);
                mainDockingDesktop.writeXML(ous);
            } catch (IOException e) {
                dockingException = new DockingException(
                        getClass().getSimpleName() + ".updatePerspective(): Unexpected Error", e);
            } finally {
                try {
                    if (ous != null) {
                        ous.close();
                    }
                    // Actually there is no effect to close baos ...
                } catch (IOException e) {
                    if (dockingException == null) {
                        dockingException = new DockingException("I/O Exception", e);
                    } else {
                        e.printStackTrace();
                    }
                }
            }
            perspective.setByteArray(baos.toByteArray());
        }
        if (dockingException != null) {
            throw dockingException;
        }
    }

    @Override
    protected void updateViews(PropertyChangeEvent evt) {
        if (evt.getSource() == this.viewFactory) {
            VlDockView newView = (VlDockView) evt.getNewValue();
            VlDockView oldView = (VlDockView) evt.getOldValue();
            if (newView == null) { // removed
                mainDockingDesktop.remove(oldView);
            } else {
                mainDockingDesktop.addDockable(newView);
            }
        }
    }

    @Override
    public void setEnabledDocking(boolean enabledDocking, JComponent dockingArea) {
        // TODO manage docking availability
        System.out.println("VlDockDockingManager.setEnabledDocking()");
    }

    @Override
    public boolean isEnabledDocking(JComponent dockingArea) {
        // TODO manage docking availability
        return true;
    }

    @Override
    public void setUndockEnabled(boolean enabled, JComponent dockingArea) {
        // TODO manage undocking possibility
        System.out.println("VlDockDockingManager.setUndockEnabled()");
    }

    @Override
    public boolean isUndockEnabled(JComponent dockingArea) {
        // TODO manage undocking possibility
        return true;
    }

    @Override
    public boolean canRestoreExactViewPosition() {
        return false;
    }
}
