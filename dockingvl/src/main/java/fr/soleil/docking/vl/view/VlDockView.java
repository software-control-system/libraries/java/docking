/*
 * This file is part of dockingvl.
 * 
 * dockingvl is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * dockingvl is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with dockingvl. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.docking.vl.view;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.Icon;

import com.vldocking.swing.docking.DockKey;
import com.vldocking.swing.docking.Dockable;
import com.vldocking.swing.docking.DockableState;
import com.vldocking.swing.docking.DockableState.Location;
import com.vldocking.swing.docking.DockingUtilities;
import com.vldocking.swing.docking.TabbedDockableContainer;
import com.vldocking.swing.docking.event.DockingActionCloseEvent;
import com.vldocking.swing.docking.event.DockingActionEvent;
import com.vldocking.swing.docking.event.DockingActionListener;

import fr.soleil.docking.event.ViewEvent;
import fr.soleil.docking.listener.IViewListener;
import fr.soleil.docking.listener.ViewListenerDelegate;
import fr.soleil.docking.view.IView;
import fr.soleil.docking.vl.SoleilDockingDesktop;

public class VlDockView implements IView, Dockable, DockingActionListener, FocusListener {

    protected Object id;
    protected boolean enabled;
    protected String title;
    protected Component component;
    protected Icon icon;
    protected DockKey key;
    protected SoleilDockingDesktop dockingDesktop;
    protected final ViewListenerDelegate delegate;

    public VlDockView(String title, Icon icon, Component component, Object id) {
        this.title = title;
        this.icon = icon;
        this.component = component;
        this.id = id;
        if (id instanceof String) {
            String stringId = (String) id;
            this.key = new DockKey(stringId, title, title, icon);
        } else {
            this.key = new DockKey(title, title, title, icon);
        }

        delegate = new ViewListenerDelegate();
        this.component.addFocusListener(this);
    }

    public void setDockingDesktop(SoleilDockingDesktop dockingDesktop) {
        this.dockingDesktop = dockingDesktop;
        dockingDesktop.registerDockable(this);
    }

    @Override
    public Component getComponent() {
        return component;
    }

    @Override
    public Object getId() {
        return id;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    @Override
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public boolean isVisible() {
        DockableState state = dockingDesktop.getDockableState(this);
        boolean visible = false;
        if (state != null) {
            Location location = state.getLocation();
            visible = (location != Location.CLOSED) && (location != Location.HIDDEN);
        }
        return visible;
    }

    @Override
    public void setVisible(boolean visible) {
        if (visible != isVisible()) {
            if (visible) {
                dockingDesktop.showDockable(this);
            } else {
                dockingDesktop.close(this);
            }
            component.setVisible(visible);
            component.setEnabled(visible);
        }
    }

    @Override
    public void select() {
        setVisible(true);
        focus(component, true);
        focusGained(new FocusEvent(component, FocusEvent.FOCUS_GAINED));
        // Following code is the one that really works
        TabbedDockableContainer tabbedDockableContainer = DockingUtilities.findTabbedDockableContainer(this);
        if (tabbedDockableContainer != null) {
            tabbedDockableContainer.setSelectedDockable(this);
        }
    }

    protected static boolean focus(Component comp, boolean forceFocus) {
        boolean focused = false;
        if (comp != null) {
            if (forceFocus || (comp.isVisible() && comp.isShowing())) {
                if (comp.isFocusable()) {
                    comp.requestFocus();
                    comp.requestFocusInWindow();
                    focused = true;
                } else if (comp instanceof Container) {
                    Container container = (Container) comp;
                    for (Component child : container.getComponents()) {
                        if (focus(child, false)) {
                            focused = true;
                            break;
                        }
                    }
                }
            }
        }
        return focused;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public Icon getIcon() {
        return icon;
    }

    @Override
    public void setIcon(Icon icon) {
        this.icon = icon;
    }

    @Override
    public Color getViewBackground() {
        return component.getBackground();
    }

    @Override
    public void setViewBackground(Color bg) {
        if (component instanceof Container) {
            List<Component> components = getAllComponents((Container) component);
            setComponentsBackground(components, bg);
        }
        component.setBackground(bg);
    }

    @Override
    public Color getViewForeground() {
        return component.getForeground();
    }

    @Override
    public void setViewForeground(Color fg) {
        component.setForeground(fg);
    }

    /**
     * Sets the background of all {@link Component}s in a given {@link List}
     * 
     * @param components The {@link List}
     * @param bg The background to set
     */
    protected void setComponentsBackground(List<?> components, Color bg) {
        if (components != null) {
            for (Object comp : components) {
                if (comp instanceof Component) {
                    ((Component) comp).setBackground(bg);
                }
            }
        }
    }

    /**
     * Sets the foreground of all {@link Component}s in a given {@link List}
     * 
     * @param components The {@link List}
     * @param bg The foreground to set
     */
    protected void setComponentsForeground(List<?> components, Color bg) {
        if (components != null) {
            for (Object comp : components) {
                if (comp instanceof Component) {
                    ((Component) comp).setForeground(bg);
                }
            }
        }
    }

    public List<Component> getAllComponents(final Container c) {
        Component[] comps = c.getComponents();
        List<Component> compList = new ArrayList<Component>();
        for (Component comp : comps) {
            compList.add(comp);
            if (comp instanceof Container) {
                compList.addAll(getAllComponents((Container) comp));
            }
        }
        return compList;
    }

    @Override
    public void setClosable(boolean closable) {
        key.setCloseEnabled(closable);
    }

    @Override
    public void addViewListener(final IViewListener listener) {
        delegate.addViewListener(listener);
    }

    @Override
    public void removeViewListener(IViewListener listener) {
        delegate.removeViewListener(listener);
    }

    @Override
    public DockKey getDockKey() {
        return key;
    }

    @Override
    public boolean acceptDockingAction(DockingActionEvent event) {
        return true;
    }

    @Override
    public void dockingActionPerformed(DockingActionEvent e) {
        if (e.getActionType() == DockingActionEvent.ACTION_CLOSE) {
            Dockable closedDockable = ((DockingActionCloseEvent) e).getDockable();
            if (getDockKey().getKey().equals(closedDockable.getDockKey().getKey())) {
                delegate.warnListeners(new ViewEvent(this, ViewEvent.VIEW_CLOSED));
            }
        }
    }

    @Override
    public void focusGained(FocusEvent e) {
        delegate.warnListeners(new ViewEvent(this, ViewEvent.FOCUS_GAINED));
    }

    @Override
    public void focusLost(FocusEvent e) {
        delegate.warnListeners(new ViewEvent(this, ViewEvent.FOCUS_LOST));
    }

}
