/*
 * This file is part of dockingvl.
 * 
 * dockingvl is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * dockingvl is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with dockingvl. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.docking.vl.view;

import java.awt.Component;

import javax.swing.Icon;
import javax.swing.JComponent;

import fr.soleil.docking.ADockingManager;
import fr.soleil.docking.view.AbstractViewFactory;
import fr.soleil.docking.view.IView;
import fr.soleil.docking.vl.SoleilDockingDesktop;
import fr.soleil.docking.vl.VlDockDockingManager;

public class VlDockViewFactory extends AbstractViewFactory {

    public VlDockViewFactory() {
        super();
    }

    @Override
    protected IView createView(String title, Icon icon, Component component, Object id) {
        IView result = new VlDockView(title, icon, component, id);
        return result;
    }

    @Override
    protected void updateViewForDockingArea(IView view, JComponent dockingArea) {
        if ((view instanceof VlDockView) && (dockingArea instanceof SoleilDockingDesktop)) {
            VlDockView vlDockView = (VlDockView) view;
            SoleilDockingDesktop dockingDesktop = (SoleilDockingDesktop) dockingArea;
            dockingDesktop.addDockable(vlDockView);
            vlDockView.setDockingDesktop(dockingDesktop);
        }
    }

    @Override
    public ADockingManager generateDockingManager() {
        return new VlDockDockingManager(this);
    }

}
