/*
 * This file is part of dockingvl.
 * 
 * dockingvl is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * dockingvl is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with dockingvl. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.docking.vl;

import java.util.HashMap;
import java.util.Map;

import com.vldocking.swing.docking.Dockable;
import com.vldocking.swing.docking.DockableState;
import com.vldocking.swing.docking.DockableState.Location;
import com.vldocking.swing.docking.DockingDesktop;
import com.vldocking.swing.docking.RelativeDockablePosition;
import com.vldocking.swing.docking.event.DockableSelectionEvent;
import com.vldocking.swing.docking.event.DockableSelectionListener;
import com.vldocking.swing.docking.event.DockingActionCloseEvent;
import com.vldocking.swing.docking.event.DockingActionEvent;
import com.vldocking.swing.docking.event.DockingActionListener;

import fr.soleil.docking.vl.view.VlDockView;

public class SoleilDockingDesktop extends DockingDesktop {

    private static final long serialVersionUID = -6192752805034560434L;

    protected final Map<String, RelativeDockablePosition> closedPositions;

    protected boolean closeEnabled;
    protected boolean autoHideEnabled;
    protected boolean maximizeEnabled;
    protected boolean floatingEnabled;

    public SoleilDockingDesktop() {
        this(true, false, true, false);
    }

    public SoleilDockingDesktop(boolean close, boolean autoHide, boolean maximize, boolean floating) {
        super();
        closedPositions = new HashMap<>();
        closeEnabled = close;
        autoHideEnabled = autoHide;
        maximizeEnabled = maximize;
        floatingEnabled = floating;
        addDockingActionListener(generateDockingActionListener());
        addDockableSelectionListener(new DockableSelectionListener() {
            @Override
            public void selectionChanged(DockableSelectionEvent dse) {
                if (dse.getSelectedDockable() instanceof VlDockView) {
                    ((VlDockView) dse.getSelectedDockable()).focusGained(null);
                }
            }
        });
    }

    @Override
    public void addDockable(Dockable dockable) {
        if (dockable != null) {
            // DockKey key = dockable.getDockKey();
            // key.setCloseEnabled(closeEnabled);
            // key.setAutoHideEnabled(autoHideEnabled);
            // key.setMaximizeEnabled(maximizeEnabled);
            // key.setFloatEnabled(floatingEnabled);

            addDockingActionListener((VlDockView) dockable);
            super.addDockable(dockable, new RelativeDockablePosition(1, 0, 0.5, 0.5));
        }
    }

    private DockingActionListener generateDockingActionListener() {
        DockingActionListener result = new DockingActionListener() {
            @Override
            public boolean acceptDockingAction(DockingActionEvent event) {
                return true;
            }

            @Override
            public void dockingActionPerformed(DockingActionEvent event) {
                if (event.getActionType() == DockingActionEvent.ACTION_CLOSE) {
                    Dockable closedDockable = ((DockingActionCloseEvent) event).getDockable();
                    DockableState state = getDockableState(closedDockable);
                    RelativeDockablePosition position = state.getPosition();
                    closedPositions.put(closedDockable.getDockKey().getKey(), position);
                }
            }
        };
        return result;
    }

    public void showDockable(Dockable dockable) {
        DockableState state = getDockableState(dockable);
        if (state == null || Location.CLOSED.equals(state.getLocation())
                || Location.HIDDEN.equals(state.getLocation())) {
            RelativeDockablePosition position = getPositionForClosedView(dockable);
            if (position == null) {
                addDockable(dockable);
            } else {
                addDockable(dockable, position);
            }
        }
    }

    private RelativeDockablePosition getPositionForClosedView(Dockable dockable) {
        RelativeDockablePosition result = null;
        if (dockable != null) {
            result = closedPositions.get(dockable.getDockKey().getKey());
        }
        return result;
    }

}
