/*
 * This file is part of dockingcore.
 * 
 * dockingcore is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * dockingcore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with dockingcore. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.docking.exception;

/**
 * An Exception thrown in case of problems with docking
 * 
 * @author girardot
 */
public class DockingException extends Exception {

    private static final long serialVersionUID = 5753611012723925293L;

    public DockingException() {
        super();
    }

    public DockingException(String message) {
        super(message);
    }

    public DockingException(Throwable cause) {
        super(cause);
    }

    public DockingException(String message, Throwable cause) {
        super(message, cause);
    }

}
