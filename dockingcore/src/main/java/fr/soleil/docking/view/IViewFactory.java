/*
 * This file is part of dockingcore.
 * 
 * dockingcore is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * dockingcore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with dockingcore. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.docking.view;

import java.awt.Component;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Collection;
import java.util.List;
import java.util.prefs.Preferences;

import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JComponent;

import fr.soleil.docking.ADockingManager;

/**
 * A factory that takes care of initializing dockable components and transmitting models.
 * 
 * @author Hardion
 * @author GIRARDOT
 */
public interface IViewFactory {

    public static final String VIEWS = "views";

    /**
     * Returns the DynamicView associated with this id (creates the
     * DynamicView).
     * 
     * @param id
     *            The id of the desired DynamicView
     * @return The DynamicView associated with this id
     */
    public IView getView(Object id);

    public Collection<IView> getViews();

    /**
     * Returns the ActionList
     * 
     * @param id
     *            The id of the desired DynamicView
     * @return The DynamicView associated with this id
     */
    public List<Action> getActionList();

    /**
     * Saves preferences
     * 
     * @param prefs preferences in which to save
     */
    public void savePreferences(Preferences prefs);

    /**
     * Loads preferences
     * 
     * @param prefs preferences to load
     */
    public void loadPreferences(Preferences prefs);

    public int size();

    /**
     * @param listener
     * @see java.beans.PropertyChangeSupport#addPropertyChangeListener(java.beans.PropertyChangeListener)
     */
    public void addPropertyChangeListener(PropertyChangeListener listener);

    /**
     * @param listener
     * @see java.beans.PropertyChangeSupport#removePropertyChangeListener(java.beans.PropertyChangeListener)
     */
    public void removePropertyChangeListener(PropertyChangeListener listener);

    public Object readViewId(ObjectInputStream in) throws IOException;

    public void writeViewId(Object id, ObjectOutputStream out) throws IOException;

    public IView addView(IView view);

    /**
     * Creates an {@link IView} for a given {@link Component}, adds it in the list of views, and
     * tries to put it in a docking area
     * 
     * @param title The {@link IView} title
     * @param icon The {@link IView} {@link Icon}
     * @param component The concerned {@link Component}
     * @param id The {@link IView} id
     * @param dockingArea The docking area
     * @return The {@link IView} if it was successfully created and added, <code>null</code> otherwise
     */
    public IView addView(String title, Icon icon, Component component, Object id, JComponent dockingArea);

    public boolean removeView(IView view);

    /**
     * Removes an {@link IView}, determined by an id, from list and from the
     * docking area.
     * 
     * @param id The id of the {@link IView} to remove.
     * @return The removed {@link IView} if such an {@link IView} existed and
     *         was successfully removed, <code>null</code> otherwise.
     */
    public IView removeView(Object id);

    /**
     * Creates a new {@link ADockingManager} based on this {@link IViewFactory}
     * 
     * @return An {@link ADockingManager}
     */
    public ADockingManager generateDockingManager();
}
