/*
 * This file is part of dockingcore.
 * 
 * dockingcore is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * dockingcore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with dockingcore. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.docking;

import java.awt.Color;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.util.List;
import java.util.prefs.Preferences;

import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JOptionPane;

import fr.soleil.docking.action.SaveDefaultPerspertiveAction;
import fr.soleil.docking.exception.DockingException;
import fr.soleil.docking.perspective.IPerspective;
import fr.soleil.docking.perspective.IPerspectiveFactory;
import fr.soleil.docking.view.IViewFactory;
import fr.soleil.lib.project.ObjectUtils;

/**
 * A class that prepares docking. Obtain your RootWindow from this class.
 * 
 * @author HARDION
 * @author GIRARDOT
 */
public abstract class ADockingManager implements PropertyChangeListener {

    private boolean automaticallySavePerspective = true;

    public boolean isAutomaticallySavePerspective() {
        return automaticallySavePerspective;
    }

    public void setAutomaticallySavePerspective(boolean automaticallySavePerspective) {
        this.automaticallySavePerspective = automaticallySavePerspective;
    }

    protected IPerspectiveFactory perspectiveFactory = null;
    protected IViewFactory viewFactory = null;

    public ADockingManager(IViewFactory viewFactory, IPerspectiveFactory perspectiveFactory) {
        this.viewFactory = viewFactory;
        this.viewFactory.addPropertyChangeListener(this);
        this.perspectiveFactory = perspectiveFactory;
        this.perspectiveFactory.addPropertyChangeListener(this);
        this.initDockingArea();
    }

    public List<Action> getActionList() {
        List<Action> result = null;
        if (viewFactory != null) {
            result = viewFactory.getActionList();
        }
        if (Boolean.getBoolean("DEBUG")) {
            result.add(new SaveDefaultPerspertiveAction(this));
        }
        return result;
    }

    /**
     * initialize this {@link ADockingManager}'s main docking area
     * 
     */
    protected abstract void initDockingArea();

    /**
     * Returns this {@link ADockingManager}'s main docking area
     * 
     * @return A {@link JComponent}
     */
    public abstract JComponent getDockingArea();

    /**
     * Creates a new docking area
     * 
     * @param areaBackground The preferred area background. May be <code>null</code>, in which case this
     *            {@link ADockingManager} chooses the {@link Color} to use
     * 
     * @return A {@link JComponent}
     */
    public abstract JComponent createNewDockingArea(Color areaBackground);

    /**
     * Changes the background of a docking area
     * 
     * @param dockingArea The docking area
     * @param areaBackground The background to set
     */
    public abstract void setDockingAreaBeackground(JComponent dockingArea, Color areaBackground);

    /**
     * Returns the perspective factory
     * 
     * @return The perspective factory
     */
    public IPerspectiveFactory getPerspectiveFactory() {
        return perspectiveFactory;
    }

    /**
     * Returns the view factory
     * 
     * @return The view factory
     */
    public IViewFactory getViewFactory() {
        return viewFactory;
    }

    public void loadPreferences(Preferences prefs) throws DockingException {
        this.perspectiveFactory.loadPreferences(prefs);
        this.viewFactory.loadPreferences(prefs);
        this.applyPerspective(perspectiveFactory.getSelectedPerspective());
    }

    public void resetLayout() throws DockingException {
        applyPerspective(perspectiveFactory.getDefault());
    }

    /**
     * Applies a perspective to the main docking area
     * 
     * @param perspective The perspective to apply
     * @throws DockingException If a problem occurred while trying to apply the perspective
     */
    public void applyPerspective(IPerspective perspective) throws DockingException {
        applyPerspective(perspective, getDockingArea());
    }

    /**
     * Applies a perspective to a docking area
     * 
     * @param perspective The perspective to apply
     * @param dockingArea The docking area
     * @throws DockingException If a problem occurred while trying to apply the perspective
     */
    public abstract void applyPerspective(IPerspective perspective, JComponent dockingArea) throws DockingException;

    public void savePreferences(Preferences prefs) throws DockingException {
        // Save current Perspective
        this.updatePerspective(this.perspectiveFactory.getSelectedPerspective());

        // Save Preferences of perspective Factory
        this.perspectiveFactory.savePreferences(prefs);

        // Save Preferences of Views
        this.viewFactory.savePreferences(prefs);

    }

    /**
     * Writes main docking area layout in a perspective
     * 
     * @param perspective The perspective
     */
    protected void updatePerspective(IPerspective perspective) {
        try {
            updatePerspective(perspective, getDockingArea());
        } catch (DockingException e) {
            JOptionPane.showMessageDialog(getDockingArea(), e.getMessage() + " (see traces)",
                    getClass().getSimpleName() + " Docking - Error", JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
        }
    }

    /**
     * Writes a docking area layout in a perspective
     * 
     * @param perspective The perspective
     * @param dockingArea The docking area
     * @throws DockingException If a problem occurred while writing the layout
     */
    public abstract void updatePerspective(IPerspective perspective, JComponent dockingArea) throws DockingException;

    public void saveDefault(File file) throws DockingException {
        this.updatePerspective(this.perspectiveFactory.getSelectedPerspective());
        this.perspectiveFactory.saveSelected(file);
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (evt != null) {
            if (ObjectUtils.sameObject(perspectiveFactory, evt.getSource())) {
                if (IPerspectiveFactory.SELECTED_PERSPECTIVE.equals(evt.getPropertyName())) {
                    IPerspective oldd = (IPerspective) evt.getOldValue();
                    IPerspective neww = (IPerspective) evt.getNewValue();
                    try {
                        if (automaticallySavePerspective) {
                            this.updatePerspective(oldd);
                        }
                        this.applyPerspective(neww);
                    } catch (DockingException e) {
                        e.printStackTrace();
                    }
                }
            } else if (ObjectUtils.sameObject(viewFactory, evt.getSource())) {
                if (IViewFactory.VIEWS.equals(evt.getPropertyName())) {
                    this.updateViews(evt);
                }
            }
        }
    }

    protected abstract void updateViews(PropertyChangeEvent evt);

    public void saveSelectedPerspective() throws DockingException {
        this.updatePerspective(this.perspectiveFactory.getSelectedPerspective());
    }

    /**
     * Enable the docking or not If the docking is disable, the inherit class must disable all
     * functionality of the docking. I must be comparable to a JPanel composed by JtabbedPane,
     * JSplitPane etc...
     * 
     * @param enabledDocking
     */
    public void setEnabledDocking(boolean enabledDocking) {
        setEnabledDocking(enabledDocking, getDockingArea());
    }

    /**
     * Get the Enabled Docking property
     * 
     * @return true if the docking is enabled
     */
    public boolean isEnabledDocking() {
        return isEnabledDocking(getDockingArea());
    }

    /**
     * Enable the docking or not in a docking area. If the docking is disable, the inherit class must disable all
     * functionality of the docking for the docking area. I must be comparable to a JPanel composed by JtabbedPane,
     * JSplitPane etc...
     * 
     * @param enabledDocking Whetehr to enable docking
     * @param dockingArea The docking area for which to enable/disable docking
     */
    public abstract void setEnabledDocking(boolean enabledDocking, JComponent dockingArea);

    /**
     * Get the Enabled Docking property for a docking area
     * 
     * @param dockingArea The docking area to test
     * 
     * @return true if the docking is enabled
     */
    public abstract boolean isEnabledDocking(JComponent dockingArea);

    /**
     * Returns whether a view, once closed, is sure to be restored at the exact previous position once shown again
     * 
     * @return A <code>boolean</code>
     */
    public abstract boolean canRestoreExactViewPosition();

    /**
     * Configures a docking area to accept or not component undocking (window creation outside of the docking area for
     * given component)
     * 
     * @param enabled Whether to accept component undocking
     * @param dockingArea The concerned docking area
     */
    public abstract void setUndockEnabled(boolean enabled, JComponent dockingArea);

    /**
     * Configures main docking area to accept or not component undocking
     * 
     * @param enabled Whether to accept component undocking
     */
    public void setUndockEnabled(boolean enabled) {
        setUndockEnabled(enabled, getDockingArea());
    }

    /**
     * Returns whether a docking area accepts component undocking (window creation outside of the docking area for
     * given component)
     * 
     * @param dockingArea The concerned docking area
     * @return A <code>boolean</code>
     */
    public abstract boolean isUndockEnabled(JComponent dockingArea);

    /**
     * Returns whether main docking area accepts component undocking
     * 
     * @return A <code>boolean</code>
     */
    public boolean isUndockEnabled() {
        return isUndockEnabled(getDockingArea());
    }

}
