/*
 * This file is part of dockingcore.
 * 
 * dockingcore is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * dockingcore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with dockingcore. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.docking.action;

import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.AbstractAction;
import javax.swing.KeyStroke;

import fr.soleil.docking.perspective.IPerspective;
import fr.soleil.docking.perspective.IPerspectiveFactory;
import fr.soleil.lib.project.ObjectUtils;

public class SelectPerspectiveAction extends AbstractAction implements PropertyChangeListener {

    private static final long serialVersionUID = 6402682388656946588L;

    private final IPerspectiveFactory factory;
    private final IPerspective perspective;

    public SelectPerspectiveAction(IPerspectiveFactory factory, IPerspective perspective) {
        super();

        this.factory = factory;
        this.factory.addPropertyChangeListener(this);
        this.perspective = perspective;

        // This is an instance initializer; it is executed just after the
        // constructor of the superclass is invoked

        // The following values are completely optional
        putValue(NAME, perspective.getName());
        // Set tool tip text
        putValue(SHORT_DESCRIPTION, "Select " + perspective.getName());

        // This text is not directly used by any Swing component;
        // however, this text could be used in a help system
        putValue(LONG_DESCRIPTION, "Select " + perspective.getName() + " as current perspective");

        // Set an icon
        // Icon icon = new ImageIcon("icon.gif");
        // putValue(Action.SMALL_ICON, icon);

        // Set a mnemonic character. In most look and feels, this causes the
        // specified character to be underlined This indicates that if the component
        // using this action has the focus and In some look and feels, this causes
        // the specified character in the label to be underlined and
        putValue(MNEMONIC_KEY, new Integer(java.awt.event.KeyEvent.VK_N));

        // Set an accelerator key; this value is used by menu items
        putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke("ctrl shift N"));

        // Set selected
        putValue(SELECTED_KEY, this.factory.getSelectedPerspective() == this.perspective);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        factory.setSelectedPerspective(perspective);
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (evt != null) {
            if (IPerspectiveFactory.SELECTED_PERSPECTIVE.equals(evt.getPropertyName())) {
                if (ObjectUtils.sameObject(evt.getNewValue(), this.perspective)) {
                    putValue(SELECTED_KEY, true);
                } else {
                    putValue(SELECTED_KEY, false);
                }
            }
        }
    }

}