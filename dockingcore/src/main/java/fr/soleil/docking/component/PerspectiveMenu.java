/*
 * This file is part of dockingcore.
 * 
 * dockingcore is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * dockingcore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with dockingcore. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.docking.component;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;

import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JMenuItem;
import javax.swing.JRadioButtonMenuItem;

import fr.soleil.docking.action.NewPerspectiveAction;
import fr.soleil.docking.perspective.IPerspectiveFactory;

public class PerspectiveMenu extends javax.swing.JMenu implements PropertyChangeListener {

    private static final long serialVersionUID = -6492472049072385936L;

    protected IPerspectiveFactory factory;

    public PerspectiveMenu(IPerspectiveFactory factory) {
        super();
        this.setText("Perspective");
        this.factory = factory;
        factory.addPropertyChangeListener(this);
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (evt != null) {
            if (evt.getSource() instanceof IPerspectiveFactory) {
                if (IPerspectiveFactory.PERSPECTIVES.equals(evt.getPropertyName())) {
                    // TODO
                }
            }
        }
    }

    @Override
    public JMenuItem add(Action a) {
        if (a instanceof NewPerspectiveAction) {
            this.addSeparator();
        }
        return super.add(a);
    }

    @Override
    public void setPopupMenuVisible(boolean flag) {
        if (flag) {
            this.removeAll();
            List<Action> actions = factory.getActionList();
            for (Action action : actions) {
                add(action);
                // JRadioButtonMenuItem item = (JRadioButtonMenuItem) this.add(action);
                // Boolean b = (Boolean) action.getValue("SELECTED_KEY");
                // if (b != null) {
                // item.setSelected(b);
                // }
            }
        }
        super.setPopupMenuVisible(flag);
    }

    @Override
    protected JMenuItem createActionComponent(Action a) {
        JMenuItem mi = new JRadioButtonMenuItem() {
            private static final long serialVersionUID = -3334691946468856486L;

            @Override
            protected PropertyChangeListener createActionPropertyChangeListener(Action a) {
                PropertyChangeListener pcl = createActionChangeListener(this);
                if (pcl == null) {
                    pcl = super.createActionPropertyChangeListener(a);
                }
                return pcl;
            }

        };
        mi.setHorizontalTextPosition(JButton.TRAILING);
        mi.setVerticalTextPosition(JButton.CENTER);
        return mi;
    }

}
