/*
 * This file is part of dockingcore.
 * 
 * dockingcore is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * dockingcore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with dockingcore. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.docking.perspective;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

@Deprecated
public class FilePerspective extends Perspective {

    public FilePerspective(String name, File file) throws IOException {
        super(name);
        if ((file != null) && (file.isFile())) {
            InputStream in = new FileInputStream(file);
            if (in != null) {
                byteArray = PerspectiveFactory.readByteArray(in);
            }
        }
    }

}
