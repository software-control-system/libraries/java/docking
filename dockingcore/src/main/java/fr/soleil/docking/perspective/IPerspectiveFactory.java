/*
 * This file is part of dockingcore.
 * 
 * dockingcore is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * dockingcore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with dockingcore. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.docking.perspective;

import java.beans.PropertyChangeListener;
import java.io.File;
import java.util.List;
import java.util.prefs.Preferences;

import javax.swing.Action;

import fr.soleil.docking.exception.DockingException;

/**
 * Factory that manages Perspectives
 * 
 * @author Hardion
 * @author GIRARDOT
 */
public interface IPerspectiveFactory {

    public static final String SELECTED_PERSPECTIVE = "selectedPerspective";
    public static final String PERSPECTIVES = "perspectives";

    /**
     * Returns the Perspective associated with this id.
     * 
     * @param id The id of the desired Perspective.
     * @return The Perspective associated with this id.
     */
    public IPerspective getPerspective(Object id);

    /**
     * Returns the selected IPerspective
     * 
     * @return The selected IPerspective
     */
    public IPerspective getSelectedPerspective();

    /**
     * Sets the selected IPerspective
     * 
     * @param selectedPerspective The IPerspective to set as selected
     */
    public void setSelectedPerspective(IPerspective selectedPerspective);

    public void setSelectedPerspective(String perspective);

    public void loadPreferences(Preferences prefs);

    public void savePreferences(Preferences prefs);

    public List<Action> getActionList();

    public void saveSelected(File file) throws DockingException;

    public void savePerspective(File file, IPerspective perspective) throws DockingException;

    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener);

    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener);

    public IPerspective getDefault();

    public IPerspective createPerspective(final String name);

    public IPerspective removePerspective(IPerspective perspective);

    public IPerspective[] getPerspectives();

    public IPerspective loadPerspectiveFromFile(File perspectiveFile, String perpectiveName) throws DockingException;

    public void loadFileInPerspective(File perspectiveFile, IPerspective perspective) throws DockingException;

    public IPerspective loadPerspectiveFromResource(String resource, String perpectiveName) throws DockingException;

    public void loadResourceInPerspective(String resource, IPerspective perspective) throws DockingException;

}
