/*
 * This file is part of dockingcore.
 * 
 * dockingcore is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * dockingcore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with dockingcore. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.docking.perspective;

/**
 * Objects that implement this interface should know where to put each dockable component.
 * 
 * @author Hardion
 * @author GIRARDOT
 */
public interface IPerspective {

    public String getName();

    /**
     * @return the byteArray
     */
    public byte[] getByteArray();

    /**
     * @param byteArray the byteArray to set
     */
    public void setByteArray(byte[] byteArray);
}
