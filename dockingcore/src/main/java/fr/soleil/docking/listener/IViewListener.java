/*
 * This file is part of dockingcore.
 * 
 * dockingcore is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * dockingcore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with dockingcore. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.docking.listener;

import java.util.EventListener;

import fr.soleil.docking.event.ViewEvent;
import fr.soleil.docking.view.IView;

/**
 * An {@link EventListener} that listens to the changes in an {@link IView}
 * 
 * @author GIRARDOT
 */
public interface IViewListener extends EventListener {

    /**
     * Notifies this {@link IViewListener} for some changes in an {@link IView}
     * 
     * @param event The {@link ViewEvent} that describes the concerned {@link IView} and changes
     */
    public void viewChanged(ViewEvent event);

}
