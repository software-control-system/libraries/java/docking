/*
 * This file is part of dockingcore.
 * 
 * dockingcore is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * dockingcore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with dockingcore. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.docking.event;

import java.util.EventObject;

import fr.soleil.docking.view.IView;

/**
 * An {@link EventObject} that describes the changes in an {@link IView}
 * 
 * @author GIRARDOT
 */
public class ViewEvent extends EventObject {

    private static final long serialVersionUID = 1725475008782759147L;

    /**
     * An <code>int</code> used to notify that the associated view was closed
     */
    public static final int VIEW_CLOSED = 0;
    /**
     * An <code>int</code> used to notify that the associated view gained the focus
     */
    public static final int FOCUS_GAINED = 1;
    /**
     * An <code>int</code> used to notify that the associated view lost the focus
     */
    public static final int FOCUS_LOST = 2;

    protected final int reason;

    public ViewEvent(IView source, int reason) {
        super(source);
        this.reason = reason;
    }

    @Override
    public IView getSource() {
        return (IView) super.getSource();
    }

    /**
     * Returns the reason of the changes
     * 
     * @return An <code>int</code>
     * @see #VIEW_CLOSED
     * @see #FOCUS_GAINED
     * @see #FOCUS_LOST
     */
    public int getReason() {
        return reason;
    }

}
