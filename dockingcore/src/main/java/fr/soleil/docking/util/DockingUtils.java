/*
 * This file is part of dockingcore.
 * 
 * dockingcore is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * dockingcore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with dockingcore. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.docking.util;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import fr.soleil.docking.view.IView;

/**
 * A class that gives some common useful methods about docking
 * 
 * @author girardot
 */
public class DockingUtils {

    /**
     * Generates an {@link AbstractAction} that can be used to display a given {@link IView}
     * 
     * @param view The concerned {@link IView}
     * @return An {@link AbstractAction}
     */
    public static AbstractAction generateShowViewAction(final IView view) {
        AbstractAction action = new AbstractAction(view.getTitle()) {

            private static final long serialVersionUID = 1387050898689913657L;

            @Override
            public void actionPerformed(ActionEvent e) {
                view.setVisible(true);
                view.select();
            }
        };
        action.putValue(AbstractAction.SMALL_ICON, view.getIcon());
        return action;
    }

}
